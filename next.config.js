module.exports = phase => ({
  rewrites: async () => [
    {
      source: '/',
      destination: '/api/echo?voice=',
    },
    {
      source: '/:voice*',
      destination: '/api/echo?voice=:voice*',
    },
  ],

  poweredByHeader: false,
})
