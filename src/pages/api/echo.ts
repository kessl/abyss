import type { NextApiRequest, NextApiResponse } from 'next'

export default (req: NextApiRequest, res: NextApiResponse) => {
  const voice = Array.isArray(req.query.voice) ? req.query.voice[0] : req.query.voice
  
  res.setHeader('Content-Type', 'text/plain; charset=utf-8')
  res.setHeader('Cache-Control', 'max-age=31536000')
  res.status(200).end(decodeURI(voice))
}
